import React, { Component } from 'react';
import Dialog, from 'material-ui/Dialog';

class FormDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.onSubmit(event)
  }

  render() {
    return (
      <Dialog>
        <form onSubmit={this.handleSubmit}>

        </form>
      </Dialog>
    )
  }
}