import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

class ProjectsWindow extends Paper {
  constructor(props) {
    super(props);

    this.handleSignOut = this.handleSignOut.bind(this)
  }

  handleSignOut() {
    this.props.userCallback('signout-success', {});
    this.props.snackBarCallback('Odhlášení proběhlo bez problémů!')
  }

  render() {
    return (
      <div>
        <h1>DevNet client</h1>
        <hr />
        <p>Přehled projektů</p>
        <RaisedButton label="Odhlásit se" onClick={this.handleSignOut}/>
      </div>
    )
  }
}

export default ProjectsWindow