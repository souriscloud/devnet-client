import axios from 'axios';
import Config from '../config/local';

const Axios = () => {
  axios.defaults.headers.common['X-Custom-AuthToken'] = 'APIAPIAPIKEY';
  return axios.create({
    baseURL: Config.apiUrl
  })
};

export default Axios;