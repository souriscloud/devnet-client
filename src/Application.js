import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
// import getMuiTheme from 'material-ui/styles/getMuiTheme';

import WindowLayout from './Components/WindowLayout';

class Application extends Component {
  render() {
    // muiTheme={getMuiTheme(darkBaseTheme)}
    return (
      <MuiThemeProvider>
        <WindowLayout/>
      </MuiThemeProvider>
    );
  }
}

export default Application;